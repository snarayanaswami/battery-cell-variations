%Battery pack analys

T_real = Pack_Current.time;
I_real = Pack_Current.signals.values;
V_real = Pack_Voltage.signals.values;
SOC_min = SOC_Pack_min_Max.signals.values(:,1);
SOC_max = SOC_Pack_min_Max.signals.values(:,3);
V_min = Voltage_pack_min_max.signals.values(:,1);
V_max = Voltage_pack_min_max.signals.values(:,2);
V_sim = V_pack_VS_Simulation.signals.values(:,2);

plot(T_real, V_min, T_real , V_max);
plot(T_real, SOC_min, T_real , SOC_max);
plot(T_real, V_real, T_real, V_sim);
plot(T_real, I_real);
grid
